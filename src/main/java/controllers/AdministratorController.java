/*
 * AdministratorController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import forms.Calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ShoutService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

    @Autowired
    private ShoutService shoutService;

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

    // dashboard ---------------------------------------------------------------

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView actionDashboard(){
	    ModelAndView result;
        Map<String, Double> statistics;

        statistics = this.shoutService.computeStatistics();

        result = new ModelAndView("administrator/dashboard");
        result.addObject("statistics", statistics);

        return result;
    }


    // chart ---------------------------------------------------------------

    @RequestMapping(value = "/chart", method = RequestMethod.GET)
    public ModelAndView actionChart(){
        ModelAndView result;
        Map<String, Double> statistics;

        statistics = this.shoutService.computeStatistics();

        result = new ModelAndView("administrator/chart");
        result.addObject("statistics", statistics);

        return result;
    }


    // inspiringQuotes ---------------------------------------------------------------

    @RequestMapping("/inspiringQuotes")
    public ModelAndView randomText() {
        ModelAndView result;

        List<String> text = new ArrayList<>();
        String text1 = "HARD WORK PAYS OFF";
        String text2 = "EARLY BIRDS WIN ALWAYS";
        String text3 = "WOKE UP AND DANCE";
        String text4 = "STEP BY STEP THE THEE GETS STRAIGHT";
        String text5 = "WORK WORK WORK!";

        text.add(text1);
        text.add(text2);
        text.add(text3);
        text.add(text4);
        text.add(text5);

        Collections.shuffle(text);
        text = text.subList(0,3);

        result = new ModelAndView("administrator/inspiringQuotes");

        result.addObject("text1",text.get(0));
        result.addObject("text2",text.get(1));
        result.addObject("text3",text.get(2));

        return result;
    }



    // Calculator ---------------------------------------------------------------

	@RequestMapping(value = "/calculator", method = RequestMethod.GET)
	public ModelAndView calculatorGet() {
		ModelAndView result;
		Calculator calculator;

		calculator = new Calculator();

		result = new ModelAndView("administrator/calculator");
		result.addObject("calculator", calculator);

		return result;
	}

	@RequestMapping(value = "/calculator", method = RequestMethod.POST)
	public ModelAndView calculatorPost(
			@Valid final Calculator calculator,
			final BindingResult binding) {

		ModelAndView result;

		calculator.compute();

		result = new ModelAndView("administrator/calculator");
		result.addObject("calculator", calculator);

		return result;
	}

}
