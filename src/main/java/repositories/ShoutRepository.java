package repositories;

import domain.Shout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
//@Transactional ** PREGUNTAR ** Cuando dejo el transactional no me despliega la pagina, pero si lo quito
// se despliega perfectamente. Es un fallo de las diapositivas o hay que ponerlo??

@Repository
public interface ShoutRepository extends JpaRepository<Shout, Integer> {

    @Query("select count(s) from Shout s")
    Long countAllShouts();

    @Query("select count(s) from Shout s where length(s.text) <= 25 ")
    Long countShortShouts();

    @Query("select count(s) from Shout s where length(s.text) > 25")
    Long countLongShouts();

}
