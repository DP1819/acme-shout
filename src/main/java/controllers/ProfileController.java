/*
 * ProfileController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import forms.Calculator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/profile")
public class ProfileController extends AbstractController {

	// Action-1 ---------------------------------------------------------------		

//	@RequestMapping("/action-1")
//	public ModelAndView action1() {
//		ModelAndView result;
//
//		result = new ModelAndView("profile/action-1");
//
//		return result;
//	}

	// Action-2 ---------------------------------------------------------------		

//	@RequestMapping("/action-2")
//	public ModelAndView action2() {
//		ModelAndView result;
//
//		result = new ModelAndView("profile/action-2");
//
//		return result;
//	}

    // inspiringQuotes ---------------------------------------------------------------

    @RequestMapping("/inspiringQuotes")
    public ModelAndView randomText() {
        ModelAndView result;

        List<String> text = new ArrayList<>();
        String text1 = "HARD WORK PAYS OFF";
        String text2 = "EARLY BIRDS WIN ALWAYS";
        String text3 = "WOKE UP AND DANCE";
        String text4 = "STEP BY STEP THE THEE GETS STRAIGHT";
        String text5 = "WORK WORK WORK!";

        text.add(text1);
        text.add(text2);
        text.add(text3);
        text.add(text4);
        text.add(text5);

        Collections.shuffle(text);
        text = text.subList(0,3);

        result = new ModelAndView("profile/inspiringQuotes");

        result.addObject("text1",text.get(0));
        result.addObject("text2",text.get(1));
        result.addObject("text3",text.get(2));

        return result;
    }


    // Calculator ---------------------------------------------------------------

	@RequestMapping(value = "/calculator", method = RequestMethod.GET)
	public ModelAndView action2Get() {
		ModelAndView result;
		Calculator calculator;

		calculator = new Calculator();

		result = new ModelAndView("profile/calculator");
		result.addObject("calculator", calculator);

		return result;
	}

	@RequestMapping(value = "/calculator", method = RequestMethod.POST)
	public ModelAndView action2Post(
			@Valid final Calculator calculator,
			final BindingResult binding) {

		ModelAndView result;

		calculator.compute();

		result = new ModelAndView("profile/calculator");
		result.addObject("calculator", calculator);

		return result;
	}


    // Action-3 ---------------------------------------------------------------

    @RequestMapping("/action-3")
    public ModelAndView action3() {
        throw new RuntimeException("Oops! An *expected* exception was thrown. This is normal behaviour.");
    }



}
