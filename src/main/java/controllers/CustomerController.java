/*
 * CustomerController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import domain.Shout;
import forms.Calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ShoutService;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	@Autowired
	private ShoutService shoutService;
	// Constructors -----------------------------------------------------------

	public CustomerController() {
		super();
	}

	// Action-1 ---------------------------------------------------------------		

//	@RequestMapping("/action-1")
//	public ModelAndView action1() {
//		ModelAndView result;
//
//		result = new ModelAndView("customer/action-1");
//
//		return result;
//	}

	// Action-2 ---------------------------------------------------------------		

//	@RequestMapping("/action-2")
//	public ModelAndView action2() {
//		ModelAndView result;
//
//		result = new ModelAndView("customer/action-2");
//
//		return result;
//	}

	// inspiringQuotes ---------------------------------------------------------------

	@RequestMapping("/inspiringQuotes")
	public ModelAndView randomText() {
		ModelAndView result;

		List<String> text = new ArrayList<>();
		String text1 = "HARD WORK PAYS OFF";
		String text2 = "EARLY BIRDS WIN ALWAYS";
		String text3 = "WOKE UP AND DANCE";
		String text4 = "STEP BY STEP THE THEE GETS STRAIGHT";
		String text5 = "WORK WORK WORK!";

		text.add(text1);
		text.add(text2);
		text.add(text3);
		text.add(text4);
		text.add(text5);

		Collections.shuffle(text);
		text = text.subList(0,3);

		result = new ModelAndView("customer/inspiringQuotes");

		result.addObject("text1",text.get(0));
		result.addObject("text2",text.get(1));
		result.addObject("text3",text.get(2));

		return result;
	}


	// Calculator ---------------------------------------------------------------

	@RequestMapping(value = "/calculator", method = RequestMethod.GET)
	public ModelAndView action2Get() {
		ModelAndView result;
		Calculator calculator;

		calculator = new Calculator();

		result = new ModelAndView("customer/calculator");
		result.addObject("calculator", calculator);

		return result;
	}

	@RequestMapping(value = "/calculator", method = RequestMethod.POST)
	public ModelAndView action2Post(
			@Valid final Calculator calculator,
			final BindingResult binding) {

		ModelAndView result;

		calculator.compute();

		result = new ModelAndView("customer/calculator");
		result.addObject("calculator", calculator);

		return result;
	}


	// Shouts ---------------------------------------------------------------

	@RequestMapping(value= "/shouts", method = RequestMethod.GET)
	public ModelAndView shouts(){
		ModelAndView result;
		Collection<Shout> shouts;

		shouts = this.shoutService.findAll();

		result = new ModelAndView("customer/shouts");
		result.addObject("shouts", shouts);

		return result;
	}

	// New shout ---------------------------------------------------------------

	@RequestMapping(value = "/newShout", method = RequestMethod.GET)
	public ModelAndView newShoutGet(){
		ModelAndView result;
		Shout shout;

		shout = this.shoutService.create();

		result = new ModelAndView("customer/newShout");
		result.addObject("shout", shout);

		return result;
	}

	@RequestMapping(value = "/newShout", method = RequestMethod.POST)
	public ModelAndView newShoutPost(
			@Valid final Shout shout,
			final BindingResult binding	){

		ModelAndView result;

		if(!binding.hasErrors()){
			this.shoutService.save(shout);
			result = new ModelAndView("redirect:shouts.do");
		} else {
			result = new ModelAndView("customer/newShout");
			result.addObject("shout", shout);
		}

		return result;

	}

}
